import React, { Component } from "react";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import AllNav from "./components/Navigation/AllNav";
import Dashboard from "./Pages/Dashboard/Dashboard";
// import StockItems from "./Pages/Reports/StockItemsReport/StockItems";
import BillHeader from "./components/BillHeader/BillHeader";
import LoginPage from "./Pages/Login/LoginPage";
import ReceiptEntry from "./Pages/ReceiptEntryPage/ReceiptEntry";
import PaymentEntry from "./Pages/PaymentEntryPage/PaymentEntry";
import AddProducts from "./Pages/Products/AddProducts";
import Creditors from "./Pages/SundryCreditors/Creditors";
import Debtors from "./Pages/SundryDebtors/Debtors";
import LandingPage from "./Pages/LandingPage/LandingPage";
import WelcomePage from "./Pages/WelcomePage/WelcomePage";
import SalesEntry from "./Pages/SalesEntryPage/SalesEntry";
import PurchaseEntry from "./Pages/PurchaseEntryPage/PurchaseEntry";
import BillInfoHeader from "./components/BillInfoHeader/BillInfoHeader";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <AllNav />
          <Switch>
            <Route path="/dashboard" exact component={Dashboard} />
            {/* <Route path="/stockItems" component={StockItems} /> */}
            <Route path="/billHeader" component={BillHeader} />
            <Route path="/loginPage" component={LoginPage} />
            <Route path="/receiptEntryPage" component={ReceiptEntry} />
            <Route path="/paymentEntryPage" component={PaymentEntry} />
            <Route path="/addProducts" component={AddProducts} />
            <Route path="/creditorsPage" component={Creditors} />
            <Route path="/debtorsPage" component={Debtors} />
            <Route path="/landingPage" component={LandingPage} />
            <Route path="/welcomePage" component={WelcomePage} />
            <Route path="/salesEntry" component={SalesEntry} />
            <Route path="/purchaseEntry" component={PurchaseEntry} />
            <Route path="/billInfoHeader" component={BillInfoHeader} />
          </Switch>
        </div>
      </Router>
    );
  }
}
export default App;
