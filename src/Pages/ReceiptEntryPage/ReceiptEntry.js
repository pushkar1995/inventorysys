import React, { Component } from "react";

import "./ReceiptEntry.css";

class ReceiptEntry extends Component {
  render() {
    return (
      <div className="mainContainerStyles">
        <header>I am header</header>
        <div className="receiptContainerStyles">
          <div className="receiptHeading">
            <h1 className="receiptTitle">Receipt Entry</h1>
          </div>
          <div className="dateContainer">
            <div>Entry Date:</div>
            <div>Receipt Date:</div>
          </div>
          <div className="dividerLine"></div>
          <div className="receiptBodyContainer">
            <div className="receiptBody">
              <div className="recptFromStyle">
                <div className="recptFromTextStyle">Receipt from:</div>
                <input type="text" className="receiptFromInputField"></input>
              </div>
              <div className="recptByStyle">
                <div className="recptByTextStyle">Receipt By:</div>
                <div className="dropdownStyle">
                  <button className="dropbtn">Receipt Type</button>
                  <div className="dropdown-content">
                    <a href="#">Cash</a>
                    <a href="#">Cheque</a>
                    <a href="#">Bank Transfer</a>
                    <a href="#">Others</a>
                  </div>
                </div>
              </div>
              <div className="recptAmountStyle">
                <div className="recptAmountTextStyle">Receipt Amount:</div>
                <input type="text" className="receiptAmountInputField"></input>
              </div>

              <div className="recptAmountWordStyle">
                <div className="recptAmountWordTextStyle">Amount in words:</div>
                <input type="text" className="recptAmtWordInputField"></input>
              </div>
              <div className="recptRemarksStyle">
                <div className="recptRemarkTextStyle">Remarks:</div>
                <input type="text" className="recptRemarksInputField"></input>
              </div>
            </div>
            <div className="dividerLine"></div>
            <div className="receiptFooter">
              <button className="cancelButtonStyle">Cancel</button>
              <button className="saveButtonStyle">Save</button>
              <button className="printButtonStyle">Print</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default ReceiptEntry;
