import React, { Component } from "react";

import "./Debtors.css";

class Debtors extends Component {
  render() {
    return (
      <div className="mainSundryDebtorsContainer">
        <header>I am header</header>
        <div className="sundryDebtorsContainerStyles">
          <div className="sundryDebtorsHeading">
            <h1 className="sundryDebtorsTitle">Sundry Debtors</h1>
          </div>

          <div className="debtorsContainer">
            <div className="debtorsBody">
              <div className="debtorsNameStyle">
                <div className="debtorsNameTextStyle">Debtors Name:</div>
                <input type="text" className="debtorsNameInputField"></input>
              </div>

              <div className="debtorsPinStyle">
                <div className="tPinTextStyle">Tpin No.:</div>
                <input type="text" className="tPinInputFieldStyle"></input>
              </div>

              <div className="addressStyle">
                <div className="addressTextStyle">Address:</div>
                <input type="text" className="addressFieldStyle"></input>
              </div>

              <div className="contactStyle">
                <div className="contactTextStyle">Contact Numbers:</div>
                <input type="text" className="contactFieldStyle"></input>
                <input type="text" className="contactFieldStyle"></input>
              </div>

              <div className="relPersonStyle">
                <div className="relPersonTextStyle">Releted Person:</div>
                <input type="text" className="relPersonFieldStyle"></input>
              </div>

              <div className="degiStyle">
                <div className="degiTextStyle">Degignation:</div>
                <input type="text" className="degiFieldStyle"></input>
              </div>
            </div>
          </div>
          <div className="dividerLine"></div>

          <div className="debtorsFooter">
            <button className="cancelButtonStyle">Cancel</button>
            <button className="saveButtonStyle">Save</button>
          </div>
        </div>
      </div>
    );
  }
}
export default Debtors;
