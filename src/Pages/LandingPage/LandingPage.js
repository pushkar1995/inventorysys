import React, { Component } from "react";

import "./LandingPage.css";
import softwareLogo from "../../Images/softwareLogo.jpg";

class LandingPage extends Component {
  render() {
    return (
      <div className="landingPageContainer">
        <img src={softwareLogo} alt="" className="softwareLogoStyle" />
      </div>
    );
  }
}
export default LandingPage;
