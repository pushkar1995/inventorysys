import React, { Component } from "react";

import "./WelcomePage.css";
import softwareLogo from "../../Images/softwareLogo.jpg";
import Logo from "../../Images/Logo.jpg";

class WelcomePage extends Component {
  render() {
    return (
      <div className="welcomePageContainer">
        <div className="welcomeHeadingContainer">
          <img src={softwareLogo} alt="" className="softwareLogoStyles" />
          <h1 className="welcomeHeading">WELCOME TO SOAD</h1>
        </div>
        <div className="plsHeading">
          <h2>Please Choose Your Company</h2>
        </div>
        <div className="companyContainer">
          <div className="companyStyle">
            <h3 className="companyName">Dhakal NonVeg Center</h3>
            <img src={Logo} alt="" className="companyLogoStyles" />
          </div>
        </div>
      </div>
    );
  }
}
export default WelcomePage;
