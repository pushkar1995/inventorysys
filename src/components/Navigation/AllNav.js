import React, { Component } from "react";

import "./AllNav.css";

import { Link } from "react-router-dom";

class AllNav extends Component {
  render() {
    const navLink = {
      color: "black",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      minheight: "10vh"
      // backgroundColor: "rgb(38, 145, 199)"
    };
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <h3>Logo</h3>
        <ul className="navbar-nav">
          <Link style={navLink} to="/dashboard">
            <li>Dashboard</li>
          </Link>
          <Link style={navLink} to="/addProducts">
            <li>Add Products</li>
          </Link>
          <Link style={navLink} to="/Reports">
            <li>Reports</li>
          </Link>
          <Link style={navLink} to="/billHeader">
            <li>Bill Header</li>
          </Link>
          <Link style={navLink} to="/loginPage">
            <li>Login</li>
          </Link>
          <Link style={navLink} to="/receiptEntryPage">
            <li>Receipt Entry</li>
          </Link>
          <Link style={navLink} to="/paymentEntryPage">
            <li>Payment Entry</li>
          </Link>
          <Link style={navLink} to="/creditorsPage">
            <li>Creditors</li>
          </Link>
          <Link style={navLink} to="/debtorsPage">
            <li>Debtors</li>
          </Link>
          <Link style={navLink} to="/landingPage">
            <li>Landing Page</li>
          </Link>
          <Link style={navLink} to="/welcomePage">
            <li>Welcome Page</li>
          </Link>
          <Link style={navLink} to="/purchaseEntry">
            <li>Purchase Entry</li>
          </Link>
          <Link style={navLink} to="/salesEntry">
            <li>Sales Entry</li>
          </Link>
          <Link style={navLink} to="/billInfoHeader">
            <li>Bill Info Header</li>
          </Link>
        </ul>
      </nav>
    );
  }
}
export default AllNav;
