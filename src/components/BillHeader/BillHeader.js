import React, { Component } from "react";

import "./BillHeader.css";
import Logo from "../../Images/Logo.jpg";

class BillHeader extends Component {
  render() {
    return (
      <div className="billHeader">
        <img src={Logo} alt="" className="logoStyles" />
        <div className="headings">
          <h1>DHAKAL NON VEG CENTER</h1>
          <h3>Pokhara-6,Lakeside,Khahare</h3>
          <div>
            <h2>VAT No.: 606482288 </h2>
            <div className="phoneNoStyles">
              <h2>061-467629</h2>
            </div>
          </div>
        </div>
        <div className="borderLine"></div>
      </div>
    );
  }
}
export default BillHeader;
